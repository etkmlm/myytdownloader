# README #


A mini YouTube tool.

## Features ##
- Download a single video with any quality, or MP3.
- Download a YouTube playlist with selecting max quality, or MP3.
- Write video infos like YouTube search in field, click search button and download.
- Multi theme support.
- Multi language support. (Soon)
- Proxy support.
- Self cleaning support.

## Screenshots ##
![Dark Main](https://i.resmim.net/j3DzH.png){width=350 height=300}
![Light Main](https://i.resmim.net/j3Tdc.png){width=350 height=300}
![Dark Video Download](https://i.resmim.net/j3mQ7.png){width=350 height=300}
![Light Video Download](https://i.resmim.net/j3XiR.png){width=350 height=300}
